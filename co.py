"""
Copyright (c) 2011 Martin Fischer - martin@fischerweb.net - martinfischer.name

Hiermit wird unentgeltlich, jeder Person, die eine Kopie der Software und der 
zugehörigen Dokumentationen (die "Software") erhält, die Erlaubnis erteilt, 
uneingeschränkt zu benutzen, inklusive und ohne Ausnahme, dem Recht, sie zu 
verwenden, kopieren, ändern, fusionieren, verlegen, verbreiten, unterlizenzieren 
und/oder zu verkaufen, und Personen, die diese Software erhalten, diese Rechte 
zu geben, unter den folgenden Bedingungen:

Der obige Urheberrechtsvermerk und dieser Erlaubnisvermerk sind in allen Kopien 
oder Teilkopien der Software beizulegen.

DIE SOFTWARE WIRD OHNE JEDE AUSDRÜCKLICHE ODER IMPLIZIERTE GARANTIE 
BEREITGESTELLT, EINSCHLIESSLICH DER GARANTIE ZUR BENUTZUNG FÜR DEN 
VORGESEHENEN ODER EINEM BESTIMMTEN ZWECK SOWIE JEGLICHER RECHTSVERLETZUNG, 
JEDOCH NICHT DARAUF BESCHRÄNKT. IN KEINEM FALL SIND DIE AUTOREN ODER 
COPYRIGHTINHABER FÜR JEGLICHEN SCHADEN ODER SONSTIGE ANSPRÜCHE HAFTBAR ZU 
MACHEN, OB INFOLGE DER ERFÜLLUNG EINES VERTRAGES, EINES DELIKTES ODER ANDERS IM 
ZUSAMMENHANG MIT DER SOFTWARE ODER SONSTIGER VERWENDUNG DER SOFTWARE ENTSTANDEN.
"""

from tkinter import *
from tkinter.ttk import *
from tkinter.messagebox import showinfo
from tkinter import filedialog
from tkinter import messagebox
import glob
import os
import shutil
import stagger

anleitung = "Hallo!\n\nWähle einen Ausgangsordner mit Unterordnern mit Dateien \
und einen Zielordner wohin diese Dateien verschoben werden sollen.\n\n\
Mp3 Dateien (Podcasts) werden direkt in die Dropbox verschoben und der ID3 Tag \
wird leicht verändert.\n\n\
Diese Software ist gedacht, um Miro Content in einen anderen Ordner zu \
verschieben und Podcasts Android-Dropbox-bereit zu machen."

ALL = W+E+N+S

class Application(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)
        if os.name == 'nt':
            root.iconbitmap("co-py.ico")
        self.master.rowconfigure(0, weight=1)
        self.master.columnconfigure(0, weight=1)
        root.title("co-py")
        self.grid(sticky=ALL)
        
        self.rowconfigure(0, weight=3)
        self.rowconfigure(0, weight=2)
        self.rowconfigure(0, weight=1)
        
        for i in range(4):
            self.columnconfigure(i, weight=1)
        
        self.menu = Menu(root, tearoff=False)
        root.config(menu=self.menu)

        self.filemenu = Menu(self.menu, tearoff=False)
        self.menu.add_cascade(label="File", menu=self.filemenu)
        self.filemenu.add_command(label="Exit", command=root.destroy)
        
        self.helpmenu = Menu(self.menu, tearoff=False)
        self.menu.add_cascade(label="Help", menu=self.helpmenu)
        self.helpmenu.add_command(label="About...", command=self.about)

        self.t1 = Text(self, bg="#A7BFCC", fg="#444", font="Droid 11 bold", 
                       padx=10, pady=10, wrap=WORD)
        self.t1.configure(height=12)
        self.t1.grid(row=0, column=0, columnspan=5, sticky=ALL)
        
        self.t1.insert(END, anleitung)
        
        self.scrollbar = Scrollbar(self, orient=VERTICAL, 
                                            command=self.t1.yview)
        self.scrollbar.grid(row=0, column=4, sticky=N+S)
        self.t1.configure(yscrollcommand=self.scrollbar.set)

        self.fr1 = Label(self)
        self.fr1.grid(row=1, column=0, columnspan=2, pady=5, padx=5, 
                      sticky=W+E+S)
        
        self.fr1.rowconfigure(0, weight=1)
        self.fr1.rowconfigure(1, weight=1)
        self.fr1.columnconfigure(0, weight=4)
        self.fr1.columnconfigure(1, weight=1)
        
        self.l1 = Label(self.fr1, text="Ausgangsordner:", width=45, 
                        anchor=CENTER, font="Droid 10")
        self.l1.grid(row=0, column=0, sticky=W+E+S)

        self.e_src = Entry(self.fr1, font="Droid 11")
        self.e_src.insert(0, "F:/Miro")
        self.e_src.grid(row=1, column=0, sticky=W+E+S)
        
        self.src_button = Button(self.fr1, text="...", width=5, 
                                 command=self.set_src)
        self.src_button.grid(row=1, column=1, sticky=W+E+S)

        self.fr2 = Label(self)
        self.fr2.grid(row=1, column=2, columnspan=2, pady=5, padx=5, 
                      sticky=W+E+S)
        
        self.fr2.rowconfigure(0, weight=1)
        self.fr2.rowconfigure(1, weight=1)
        self.fr2.columnconfigure(0, weight=4)
        self.fr2.columnconfigure(1, weight=1)
        
        self.l2 = Label(self.fr2, 
                        text="Zielordner:", width=45, 
                        anchor=CENTER, font="Droid 10")
        self.l2.grid(row=0, column=0, sticky=W+E+S)
    
        self.e_dst = Entry(self.fr2, font="Droid 11")
        self.e_dst.insert(0, "F:/My Documents/My Videos/TV/C Tube")
        self.e_dst.grid(row=1, column=0, sticky=W+E+S)

        self.dst_button = Button(self.fr2, text="...", width=5, 
                                 command=self.set_dst)
        self.dst_button.grid(row=1, column=1, sticky=W+E+S)
        
        self.b_move = Button(self, text="Dateien verschieben", 
                             command=self.move_files).grid(row=2, column=0, 
                                                           pady=5, padx=5, 
                                                           sticky=W+E+S)
        self.b_del = Button(self, text="Ordner löschen", 
                            command=self.delete_dirs).grid(row=2, column=1, 
                                                           pady=5, padx=5, 
                                                           sticky=W+E+S) 
        self.b_reset = Button(self, text="Reset", 
                              command=self.reset).grid(row=2, column=2, 
                                                       pady=5, padx=5, 
                                                       sticky=W+E+S)
        self.b_quit = Button(self, text="Schließen", 
                             command=root.destroy).grid(row=2, column=3, 
                                                        pady=5, padx=5, 
                                                        sticky=W+E+S)

    def set_src(self):
        dirname = filedialog.askdirectory(initialdir=os.path.expanduser('~'),
                                        title="Ausgangsordner")
        if dirname:
            self.e_src.delete(0, END)
            self.e_src.insert(0, dirname)

    def set_dst(self):
        dirname = filedialog.askdirectory(initialdir=os.path.expanduser('~'),
                                        title="Zielordner")
        if dirname:
            self.e_dst.delete(0, END)
            self.e_dst.insert(0, dirname)

    def move_files(self):
        t1_inhalt = self.t1.get(1.0, 1.6)
        if t1_inhalt == "Hallo!":
            self.t1.delete(1.0, END)
        src = self.e_src.get() + "/*"
        dst = self.e_dst.get()
        self.files = glob.glob(os.path.join(src, "*.*"))
        for filename in self.files:
            self.filename = os.path.normpath(filename)
            if self.filename.split(".")[-1] == "mp3":
                try:
                    tag = stagger.read_tag(self.filename)
                    old_tag = tag.album
                    self.t1.insert(END, 
                            "--Ändere mp3 ID3 von \"{}\" auf \"Podcast\"\n"\
                            .format(old_tag))
                    tag.album = "Podcast"
                    tag.write()
                    
                    if tag.title:
                        self.new_filename = ""
                        for i in self.filename.split("\\")[:-1]:
                            self.new_filename += i + "\\"
                        self.new_filename += tag.title + ".mp3"
                        os.rename(self.filename, self.new_filename)
                        
                        self.t1.insert(END, "-Verschiebe: {}\n".format(
                                            self.new_filename.split("\\")[-1]))
                        self.update()
                        shutil.move(self.new_filename, 
                                    "F:/My Documents/My Dropbox/radio")
                    else:
                        self.t1.insert(END, "-Verschiebe: {}\n".format(
                                                self.filename.split("\\")[-1]))
                        self.update()
                        shutil.move(self.filename, 
                                    "F:/My Documents/My Dropbox/radio")
                        
                except Exception as err:
                    self.t1.insert(END, "---Error bei {}: {}\n".format(
                                            self.filename.split("\\")[-1], 
                                            err))
                    try:
                        self.t1.insert(END, "-Verschiebe: {}\n".format(
                                            self.filename.split("\\")[-1]))
                        self.update()
                        shutil.move(self.filename, 
                                    "F:/My Documents/My Dropbox/radio")
                    except Exception as err2:
                        self.t1.insert(END, "---Error bei {}: {}\n".format(
                                                self.filename.split("\\")[-1], 
                                                err2))
                    
            else:    
                self.t1.insert(END, "-Verschiebe: {}\n".format(
                                                self.filename.split("\\")[-1]))
                self.update()
                shutil.move(self.filename, dst)
        
        if self.files:    
            self.t1.insert(END, "{}\n".format("*"*80))
            self.t1.insert(END, "Verschieben fertig\n")
            self.t1.insert(END, "{}\n".format("*"*80))
        else:
            self.t1.insert(END, "{}\n".format("*"*80))
            self.t1.insert(END, "Nichts zu verschieben\n")
            self.t1.insert(END, "{}\n".format("*"*80))            

    def delete_dirs(self):
        src = self.e_src.get() + "/*"
        delete = messagebox.askyesno(
            message="Alle Ordner im Ausgangsordner \"{}\" wirklich löschen?"\
                .format(src[:-2]),
                icon="question", title="Ordner löschen?")            
        if delete:
            t1_inhalt = self.t1.get(1.0, 1.6)
            if t1_inhalt == "Hallo!":
                self.t1.delete(1.0, END)
            dirs = glob.glob(src)
            for dirname in dirs:
                dirname = os.path.normpath(dirname)
                if dirname == "F:\\Miro\\Incomplete Downloads":
                    continue
                else:
                    shutil.rmtree(dirname)
            if len(dirs) > 1:
                self.t1.insert(END, "{}\n".format("*"*80))    
                self.t1.insert(END, "Ordner gelöscht\n")
                self.t1.insert(END, "{}\n".format("*"*80))
            else:
                self.t1.insert(END, "{}\n".format("*"*80))    
                self.t1.insert(END, "Keine Ordner zu löschen\n")
                self.t1.insert(END, "{}\n".format("*"*80))            
        
    def about(self):
        showinfo("co-py", "co-py\n\nAutor: Martin Fischer\n\nE-Mail: martin@fischerweb.net")
        
    def reset(self):
        self.t1.delete(1.0, END)
        self.t1.insert(END, anleitung)
        self.e_src.delete(0, END)
        self.e_dst.delete(0, END)
        self.e_src.insert(0, r"F:/Miro")
        self.e_dst.insert(0, r"F:/My Documents/My Videos/TV/C Tube")

if __name__=="__main__":    
    root = Tk()
    app = Application(master=root)
    app.mainloop()
