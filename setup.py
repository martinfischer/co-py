"""
Windows cx_freeze script für pystunden

cd zu diesem Ordner - dann "python setup.py build"
"""
from cx_Freeze import setup, Executable

includes = []
excludes = []
packages = []
path = []
includefiles = ["co-py.ico"]

build_target = Executable(
    script = "co.py",
    initScript = None,
    base = "Win32GUI",
    targetDir = None,
    targetName = "co-py.exe",
    compress = True,
    copyDependentFiles = True,
    appendScriptToExe = False,
    appendScriptToLibrary = False,
    icon = "co-py.ico",
    )

setup(
    name = "co-py",
    version = "0.1",
    description = "Moving my files",
    author = "Martin Fischer",
    author_email="martin@fischerweb.net",
    options = {"build_exe": {"includes": includes,
                             "excludes": excludes,
                             "packages": packages,
                             "path": path,
                             "include_files": includefiles,
                             }
               },
    executables = [build_target])
